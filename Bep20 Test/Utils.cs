﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bep20_Test
{
    class Utils
    {
        public static void require(bool _is, string error)
        {
            if (_is)
                return;
            else
                throw new Exception(error);
        }
    }
}
