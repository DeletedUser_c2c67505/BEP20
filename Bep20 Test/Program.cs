﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Bep20_Test
{
    internal class Program
    {
        static void Main()
        {
            BoostToken token = new();

            while (true)
            {
                var line = Console.ReadLine();
                var parts = line.Split('-');

                int count = 0;
                List<ulong> args = new List<ulong>();

                foreach (var part in parts)
                {
                    if (count++ == 0)
                        continue;

                    args.Add(ulong.Parse(part));
                }

                switch (parts[0])
                {
                    case "create":
                        token.create((UIntPtr)args[0]);
                        break;
                    case "context":
                        token.msg.sender = (UIntPtr)args[0];
                        break;
                    case "list":
                        foreach (var item in token.getHolders())
                        {
                            Console.WriteLine($"User: {item.Key} Balance: {item.Value}");
                        }
                        break;
                    case "transfer":
                        Console.WriteLine(token.transfer((UIntPtr)args[0], args[1]) ? "Transfered" : "Transfer - Failed");
                        break;
                    case "approve":
                        Console.WriteLine(token.approve((UIntPtr)args[0], args[1]) ? "Approved" : "Approve - Failed");
                        break;
                    case "transferFrom":
                        Console.WriteLine(token.transferFrom((UIntPtr)args[0], (UIntPtr)args[1], args[2]) ? "Transfered" : "Transfer - Failed");
                        break;
                    case "balanceOf":
                        Console.WriteLine("Balance is " + token.balanceOf((UIntPtr)args[0]));
                        break;
                    case "allowance":
                        Console.WriteLine("Allowance is " + token.allowance((UIntPtr)args[0], (UIntPtr)args[1]));
                        break;
                    case "increaseAllowance":
                        Console.WriteLine(token.increaseAllowance((UIntPtr)args[0], args[1]) ? "Allowance Increased" : "Allowance Increase - Failed");
                        break;
                    case "decreaseAllowance":
                        Console.WriteLine(token.decreaseAllowance((UIntPtr)args[0], args[1]) ? "Allowance Decreased" : "Allowance Decrease - Failed");
                        break;
                    case "totalFees":
                        Console.WriteLine("Total Fees is " + token.totalFees());
                        break;
                    case "totalBurn":
                        Console.WriteLine("Total Burn is " + token.totalBurn());
                        break;
                    default:
                        Console.WriteLine("Parse - Failed");
                        break;
                }
            }
            
            Task.Delay(-1).Wait();
        }
    }
}
