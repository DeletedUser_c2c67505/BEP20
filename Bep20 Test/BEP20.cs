﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Bep20_Test
{

    public struct Message
    {
        public UIntPtr sender = (UIntPtr)0x01;
        public Byte[] data;
    }

    public abstract class Context
    {
        public Message msg = new Message();
        public UIntPtr _msgSender()
        {
            return msg.sender;
        }

        public Byte[] _msgData()
        {
            return msg.data;
        }
    }

    public interface IBEP20
    {
        public abstract UInt64 totalSupply();
        public abstract UInt64 balanceOf(UIntPtr account);
        public abstract bool transfer(UIntPtr recipient, UInt64 amount);
        public abstract UInt64 allowance(UIntPtr owner, UIntPtr spender);
        public abstract bool approve(UIntPtr spender, UInt64 amount);
        public abstract bool transferFrom(UIntPtr spender, UIntPtr recipient, UInt64 amount);
    }

    public class Ownable : Context
    {
        private UIntPtr _owner;

        public delegate void OwnershipTransferredEventHandler(UIntPtr previousOwner, UIntPtr newOwner);
        public event OwnershipTransferredEventHandler OwnershipTransferred;

        public Ownable()
        {
            var msgSender = _msgSender();
            _owner = msgSender;
            OwnershipTransferred?.Invoke(UIntPtr.Zero, msgSender);
        }

        public UIntPtr owner()
        {
            return _owner;
        }

        public void onlyOwner()
        {
            Utils.require(_owner == _msgSender(), "Ownable: caller is not the owner");
        }

        public void renounceOwnership()
        {
            onlyOwner();

            OwnershipTransferred?.Invoke(_owner, UIntPtr.Zero);
            _owner = UIntPtr.Zero;
        }

        public void transferOwnership(UIntPtr newOwner)
        {
            onlyOwner();

            Utils.require(newOwner != UIntPtr.Zero, "Ownable: new owner is the zero address");
            OwnershipTransferred?.Invoke(_owner, newOwner);
            _owner = newOwner;
        }
    }

    class BoostToken : Ownable, IBEP20
    {
        public delegate void TransferEventHandler(UIntPtr from, UIntPtr to, UInt64 amount);
        public event TransferEventHandler Transfer;

        public delegate void ApprovalEventHandler(UIntPtr owner, UIntPtr spender, UInt64 value);
        public event ApprovalEventHandler Approval;

        private Dictionary<UIntPtr, UInt64> _rOwned = new Dictionary<UIntPtr, ulong>();
        private Dictionary<UIntPtr, UInt64> _tOwned = new Dictionary<UIntPtr, ulong>();
        private Dictionary<UIntPtr, Dictionary<UIntPtr, UInt64>> _allowances = new Dictionary<UIntPtr, Dictionary<UIntPtr, ulong>>();

        private Dictionary<UIntPtr, bool> _isExcludedFromFee = new Dictionary<UIntPtr, bool>();
        private Dictionary<UIntPtr, bool> _isExcludedFromReward = new Dictionary<UIntPtr, bool>();
        private Dictionary<UIntPtr, bool> _blacklist = new Dictionary<UIntPtr, bool>();

        private List<UIntPtr> _excludedFromReward = new List<UIntPtr>();

        private string _NAME = "Gold";
        private string _SYMBOL = "GLD";
        private uint _DECIMALS = 12;

        private const UInt64 _MAX = UInt64.MaxValue;
        private const UInt64 _GRANULARITY = 100;

        private UInt64 _tTotal = (ulong)(10000000 * Math.Pow(10, 12));
        private UInt64 _rTotal = (ulong)(_MAX - (_MAX % (10000000 * Math.Pow(10, 12))));

        private UInt64 _tFeeTotal;
        private UInt64 _tBurnTotal;

        public UInt64 _TAX_FEE = 500;
        private UInt64 _previousTaxFee = 500;

        public UInt64 _BURN_FEE = 500;
        private UInt64 _previousBurnFee = 500;

        public BoostToken()
        {
            _rOwned[_msgSender()] = _rTotal;
            _tOwned[_msgSender()] = _rTotal;
            Transfer?.Invoke(UIntPtr.Zero, _msgSender(), _tTotal);

            _isExcludedFromFee[owner()] = true;
            _isExcludedFromReward[owner()] = false;
            _allowances[owner()] = new Dictionary<UIntPtr, ulong>();
        }

        // test
        public void create(UIntPtr address)
        {
            _rOwned[address] = 0;
            _tOwned[address] = 0;
            Transfer?.Invoke(UIntPtr.Zero, address, 0);

            _isExcludedFromFee[address] = false;
            _isExcludedFromReward[address] = true;
            _allowances[address] = new Dictionary<UIntPtr, ulong>();
        }

        public Dictionary<UIntPtr, ulong> getHolders()
        {
            return _rOwned;
        }

        public string name()
        {
            return _NAME;
        }

        public string symbol()
        {
            return _SYMBOL;
        }

        public uint decimals()
        {
            return _DECIMALS;
        }

        public UInt64 totalSupply()
        {
            return _tTotal;
        }

        public UInt64 balanceOf(UIntPtr account)
        {
            if (_isExcludedFromReward[account]) return _tOwned[account];
            return tokenFromMagnetion(_rOwned[account]);
        }

        public bool transfer(UIntPtr recipient, UInt64 amount)
        {
            _transfer(_msgSender(), recipient, amount);
            return true;
        }

        public UInt64 allowance(UIntPtr owner, UIntPtr spender)
        {
            return _allowances[owner][spender];
        }

        public bool approve(UIntPtr spender, UInt64 amount)
        {
            _approve(_msgSender(), spender, amount);
            return true;
        }

        public bool transferFrom(UIntPtr sender, UIntPtr recipient, UInt64 amount)
        {
            _transfer(sender, recipient, amount);
            _approve(sender, _msgSender(), _allowances[sender][_msgSender()] - amount);
            return true;
        }

        public bool increaseAllowance(UIntPtr spender, UInt64 addedValue)
        {
            _approve(_msgSender(), spender, _allowances[_msgSender()][spender] + addedValue);
            return true;
        }

        public bool decreaseAllowance(UIntPtr spender, UInt64 subtractedValue)
        {
            _approve(_msgSender(), spender, _allowances[_msgSender()][spender] - subtractedValue);
            return true;
        }

        public bool isExcludedFromReward(UIntPtr account)
        {
            return _isExcludedFromReward[account];
        }

        public UInt64 totalFees()
        {
            return _tFeeTotal;
        }

        public UInt64 totalBurn()
        {
            return _tBurnTotal;
        }

        public void deliver(UInt64 tAmount)
        {
            UIntPtr sender = _msgSender();
            Utils.require(!_isExcludedFromReward[sender], "Excluded addresses cannot call this function");
            UInt64 rAmount = _getValues(tAmount).Item1;
            _rOwned[sender] = _rOwned[sender] - rAmount;
            _rTotal = _rTotal - rAmount;
            _tFeeTotal = _tFeeTotal + tAmount;
        }

        public UInt64 magnetionFromToken(UInt64 tAmount, bool deductTransferFee)
        {
            Utils.require(tAmount <= _tTotal, "Amount must be less than supply");
            if(!deductTransferFee)
            {
                UInt64 rAmount = _getValues(tAmount).Item1;
                return rAmount;
            }
            else
            {
                UInt64 rTransferAmount = _getValues(tAmount).Item2;
                return rTransferAmount;
            }
        }

        public UInt64 tokenFromMagnetion(UInt64 rAmount)
        {
            Utils.require(rAmount <= _rTotal, "Amount must be less than total magnetions");
            UInt64 currentRate = _getRate();
            return rAmount / currentRate;
        }

        public void excludeFromReward(UIntPtr account)
        {
            onlyOwner();

            Utils.require(!_isExcludedFromReward[account], "Account is already excluded");
            if(_rOwned[account] > 0)
            {
                _tOwned[account] = tokenFromMagnetion(_rOwned[account]);
            }
            _isExcludedFromReward[account] = true;
            _excludedFromReward.Add(account);
        }

        public void includeInReward(UIntPtr account)
        {
            onlyOwner();

            Utils.require(_isExcludedFromReward[account], "Account is already included");
            _excludedFromReward.Remove(account);
            _tOwned[account] = 0;
            _isExcludedFromReward[account] = false;
        }

        public bool isBlacklisted(UIntPtr account)
        {
            if(account == owner() || account == _msgSender())
            {
                return false;
            }
            return _blacklist[account];
        }

        public void setBlacklistStatus(UIntPtr account, bool status)
        {
            onlyOwner();

            _blacklist[account] = status;
        }

        private void removeAllFee()
        {
            if (_TAX_FEE == 0 && _BURN_FEE == 0) return;

            _previousTaxFee = _TAX_FEE;
            _previousBurnFee = _BURN_FEE;
            _TAX_FEE = 0;
            _BURN_FEE = 0;
        }

        private void restoreAllFee()
        {
            _TAX_FEE = _previousTaxFee;
            _BURN_FEE = _previousBurnFee;
        }

        public void excludeFromFee(UIntPtr account)
        {
            onlyOwner();

            _isExcludedFromFee[account] = true;
        }

        public void includeInFee(UIntPtr account)
        {
            onlyOwner();

            _isExcludedFromFee[account] = false;
        }

        public bool isExcludedFromFee(UIntPtr account)
        {
            return _isExcludedFromFee[account];
        }

        private void _approve(UIntPtr owner, UIntPtr spender, UInt64 amount)
        {
            Utils.require(owner != UIntPtr.Zero, "BEP20: approve from the zero address");
            Utils.require(spender != UIntPtr.Zero, "BEP20: approve to the zero address");

            _allowances[owner][spender] = amount;
            Approval?.Invoke(owner, spender, amount);
        }

        private void recieve() { }

        private void _transfer(UIntPtr sender, UIntPtr recipient, UInt64 amount)
        {
            Utils.require(sender != UIntPtr.Zero, "BEP20: transfer from the zero address");
            Utils.require(recipient != UIntPtr.Zero, "BEP20: transfer to the zero address");
            Utils.require(amount > 0, "Transfer amount must be greater than zero");
            Utils.require(isBlacklisted(sender) == false, "You are in blacklist");

            bool takeFee = true;

            if (_isExcludedFromFee[sender] || _isExcludedFromFee[recipient])
            {
                takeFee = false;
            }

            _tokenTransfer(sender, recipient, amount, takeFee);
        }

        private void _tokenTransfer(UIntPtr sender, UIntPtr recipient, UInt64 amount, bool takeFee)
        {
            if (!takeFee)
                removeAllFee();

            if (_isExcludedFromReward[sender] && !_isExcludedFromReward[recipient])
            {
                _transferFromExcluded(sender, recipient, amount);
            }
            else if (!_isExcludedFromReward[sender] && _isExcludedFromReward[recipient])
            {
                _transferToExcluded(sender, recipient, amount);
            }
            else if (!_isExcludedFromReward[sender] && !_isExcludedFromReward[recipient])
            {
                _transferStandard(sender, recipient, amount);
            }
            else if (_isExcludedFromReward[sender] && _isExcludedFromReward[recipient])
            {
                _transferBothExcluded(sender, recipient, amount);
            }
            else
            {
                _transferStandard(sender, recipient, amount);
            }

            if (!takeFee)
                restoreAllFee();
        }

        private void _transferStandard(UIntPtr sender, UIntPtr recipient, UInt64 tAmount)
        {
            UInt64 currentRate = _getRate();
            (UInt64 rAmount, UInt64 rTransferAmount, UInt64 rFee, UInt64 tTransferAmount, UInt64 tFee, UInt64 tBurn) = _getValues(tAmount);
            UInt64 rBurn = tBurn * currentRate;
            _rOwned[sender] = _rOwned[sender] - rAmount;
            _rOwned[recipient] = _rOwned[recipient] + rTransferAmount;
            _magnetFee(rFee, rBurn, tFee, tBurn);
            Transfer?.Invoke(sender, recipient, tTransferAmount);
        }

        private void _transferToExcluded(UIntPtr sender, UIntPtr recipient, UInt64 tAmount)
        {
            UInt64 currentRate = _getRate();
            (UInt64 rAmount, UInt64 rTransferAmount, UInt64 rFee, UInt64 tTransferAmount, UInt64 tFee, UInt64 tBurn) = _getValues(tAmount);
            UInt64 rBurn = tBurn * currentRate;
            _rOwned[sender] = _rOwned[sender] - rAmount;
            _tOwned[recipient] = _tOwned[recipient] + tTransferAmount;
            _rOwned[recipient] = _rOwned[recipient] + rTransferAmount;
            _magnetFee(rFee, rBurn, tFee, tBurn);
            Transfer?.Invoke(sender, recipient, tTransferAmount);
        }

        private void _transferFromExcluded(UIntPtr sender, UIntPtr recipient, UInt64 tAmount)
        {
            UInt64 currentRate = _getRate();
            (UInt64 rAmount, UInt64 rTransferAmount, UInt64 rFee, UInt64 tTransferAmount, UInt64 tFee, UInt64 tBurn) = _getValues(tAmount);
            UInt64 rBurn = tBurn * currentRate;
            _tOwned[sender] = _tOwned[sender] - tAmount;
            _rOwned[sender] = _rOwned[sender] - rAmount;
            _rOwned[recipient] = _rOwned[recipient] + rTransferAmount;
            _magnetFee(rFee, rBurn, tFee, tBurn);
            Transfer?.Invoke(sender, recipient, tTransferAmount);
        }

        private void _transferBothExcluded(UIntPtr sender, UIntPtr recipient, UInt64 tAmount)
        {
            UInt64 currentRate = _getRate();
            (UInt64 rAmount, UInt64 rTransferAmount, UInt64 rFee, UInt64 tTransferAmount, UInt64 tFee, UInt64 tBurn) = _getValues(tAmount);
            UInt64 rBurn = tBurn * currentRate;
            _tOwned[sender] = _tOwned[sender] - tAmount;
            _rOwned[sender] = _rOwned[sender] - rAmount;
            _tOwned[recipient] = _tOwned[recipient] + tTransferAmount;
            _rOwned[recipient] = _rOwned[recipient] + rTransferAmount;
            _magnetFee(rFee, rBurn, tFee, tBurn);
            Transfer?.Invoke(sender, recipient, tTransferAmount);
        }

        private void _magnetFee(UInt64 rFee, UInt64 rBurn, UInt64 tFee, UInt64 tBurn)
        {
            _rTotal = _rTotal - rFee - rBurn;
            _tFeeTotal = _tFeeTotal + tFee;
            _tBurnTotal = _tBurnTotal + tBurn;
            _tTotal = _tTotal - tBurn;
        }

        private (UInt64, UInt64, UInt64, UInt64, UInt64, UInt64) _getValues(UInt64 tAmount)
        {
            (UInt64 tTransferAmount, UInt64 tFee, UInt64 tBurn) = _getTValues(tAmount, _TAX_FEE, _BURN_FEE);
            UInt64 currentRate = _getRate();
            (UInt64 rAmount, UInt64 rTransferAmount, UInt64 rFee) = _getRValues(tAmount, tFee, tBurn, currentRate);
            return (rAmount, rTransferAmount, rFee, tTransferAmount, tFee, tBurn);
        }

        private (UInt64, UInt64, UInt64) _getTValues(UInt64 tAmount, UInt64 taxFee, UInt64 burnFee)
        {
            UInt64 tFee = tAmount * taxFee / _GRANULARITY / 100;
            UInt64 tBurn = tAmount * burnFee / _GRANULARITY / 100;
            UInt64 tTransferAmount = tAmount - tFee - tBurn;
            return (tTransferAmount, tFee, tBurn);
        }

        private (UInt64, UInt64, UInt64) _getRValues(UInt64 tAmount, UInt64 tFee, UInt64 tBurn, UInt64 currentRate)
        {
            UInt64 rAmount = tAmount * currentRate;
            UInt64 rFee = tFee * currentRate;
            UInt64 rBurn = tBurn * currentRate;
            UInt64 rTransferAmount = rAmount - rFee - rBurn;
            return (rAmount, rTransferAmount, rFee);
        }

        private UInt64 _getRate()
        {
            (UInt64 rSupply, UInt64 tSupply) = _getCurrentSupply();
            return rSupply / tSupply;
        }

        private (UInt64, UInt64) _getCurrentSupply()
        {
            UInt64 rSupply = _rTotal;
            UInt64 tSupply = _tTotal;
            for (int i = 0; i < _excludedFromReward.Count; i++)
            {
                if (_rOwned[_excludedFromReward[i]] > rSupply || _tOwned[_excludedFromReward[i]] > tSupply) return (_rTotal, _tTotal);
                rSupply = rSupply - _rOwned[_excludedFromReward[i]];
                tSupply = tSupply - _tOwned[_excludedFromReward[i]];
            }
            if (rSupply < _rTotal / _tTotal) return (_rTotal, _tTotal);
            return (rSupply, tSupply);
        }
    }
}
